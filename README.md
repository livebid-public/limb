# Livebid Image Manipulation Bot (LIMB)

LIMB is a Node.js application that processes and manipulates images.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed Node.js on your machine.

## Installing Node.js on Windows

1. Download the Windows installer from the [Node.js downloads page](https://nodejs.org/en/download/).
2. Run the installer (the .msi file you downloaded in the first step.)
3. Follow the prompts in the installer (Accept the license agreement, click the NEXT button a bunch of times and accept the default installation settings).
4. Restart your computer. You won’t be able to run Node.js until you restart your computer.

You can test it by opening PowerShell and running:

```sh
node -v
```

The version of Node.js installed will be displayed.

## Installing LIMB

1. Navigate to the project directory:

```sh
cd limb
```

2. Install the dependencies:

```sh
npm install
```

## Running LIMB

To run LIMB, follow these steps:

```sh
node src/index.js
```

## Config flags

You can pass the following flags to the application:

- `--mode`:
  - `pair` - Process a pair of images and join them.
  - `single` - Process a single image.
- `--removeBg`: Remove the background from the image. (Slow)
- `--flipX`: Flip the image horizontally.
- `--flipY`: Flip the image vertically.
- `--debug`: Enable debug mode.
- `--inPath`: Path to the input image.
- `--outPath`: Path to the output image.
- `--debugPath`: Path to the debug image.

## Contact

If you want to contact the author, you can reach out at `info@livebid.cz`

## License

This project is maintained by [Livebid](https://www.livebid.cz/) under closed source license.
