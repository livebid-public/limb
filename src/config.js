export const config = {
  debug: true,

  // Paths
  inPath: "./data/in",
  outPath: "./data/out",
  debugPath: "./data/debug",

  // Config
  padding: 64,
  flipImageX: false,
  flipImageY: false,
  removeBg: false,
};

export const FORBIDDEN_FILES = [".DS_Store", "Thumbs.db", "desktop.ini"];

// Downscale image to this size to speed up detection
export const DETECTOR_MAX_IMAGE_SIZE = 1024;
// Block size of scanned parts of image to detect if it's light or dark
export const DETECTOR_BLOCK_SIZE = 4;

// Threshold to consider block as white (white color averaged in block) => 0-255
export const DETECTOR_WHITE_THRESHOLD = 12;

// Minimum amount of light blocks around to consider block as light (otherwise it's random)
export const AROUND_MIN_COUNT = 12;
// Distance around block to check if it's random
export const AROUND_DISTANCE = DETECTOR_BLOCK_SIZE * 4;
