import {
  AROUND_DISTANCE,
  AROUND_MIN_COUNT,
  DETECTOR_BLOCK_SIZE,
  DETECTOR_MAX_IMAGE_SIZE,
  DETECTOR_WHITE_THRESHOLD,
} from "../config.js";
import { createPerfLog } from "../utils.js";

export async function detectImageBoundaries(image) {
  if (!image) throw new Error("No image provided");
  if (!image.bitmap) throw new Error("No image bitmap provided");
  if (!AROUND_DISTANCE) throw new Error("No AROUND_DISTANCE provided");
  if (!AROUND_MIN_COUNT) throw new Error("No AROUND_MIN_COUNT provided");
  if (!DETECTOR_BLOCK_SIZE) throw new Error("No DETECTOR_BLOCK_SIZE provided");
  if (!DETECTOR_MAX_IMAGE_SIZE) throw new Error("No DETECTOR_MAX_IMAGE_SIZE provided");
  if (!DETECTOR_WHITE_THRESHOLD) throw new Error("No DETECTOR_WHITE_THRESHOLD provided");

  const { width, height } = image.bitmap;
  const maxDim = Math.max(width, height);
  const downscaledWidth = Math.floor((width / maxDim) * DETECTOR_MAX_IMAGE_SIZE);
  const downscaledHeight = Math.floor((height / maxDim) * DETECTOR_MAX_IMAGE_SIZE);

  const upscaleX = width / downscaledWidth;
  const upscaleY = height / downscaledHeight;

  const logger = createPerfLog();

  image = image.clone();
  image.resize(downscaledWidth, downscaledHeight);
  image.greyscale();
  image.brightness(0.4);
  image.contrast(0.4);
  image.convolution([
    [0, 1, 0],
    [1, -4, 1],
    [0, 1, 0],
  ]);

  logger.log("Preprocess");

  const postprocessedImage = image;
  image = image.clone();

  const blocks = [];
  const blockClusters = {};

  const CLUSTER_SIZE = 8;

  const saveToCluster = (x, y) => {
    const cx = Math.floor(x / DETECTOR_BLOCK_SIZE / CLUSTER_SIZE);
    const cy = Math.floor(y / DETECTOR_BLOCK_SIZE / CLUSTER_SIZE);

    const radius = 1;

    for (let dx = -radius; dx <= radius; dx++) {
      for (let dy = -radius; dy <= radius; dy++) {
        const key = `${cx + dx},${cy + dy}`;
        blockClusters[key] ??= [];
        blockClusters[key].push({ x, y });
      }
    }
  };

  for (let x = 0; x < downscaledWidth; x += DETECTOR_BLOCK_SIZE) {
    for (let y = 0; y < downscaledHeight; y += DETECTOR_BLOCK_SIZE) {
      if (x + DETECTOR_BLOCK_SIZE > downscaledWidth || y + DETECTOR_BLOCK_SIZE > downscaledHeight) {
        continue;
      }

      // const block = image.clone().crop(x, y, DETECTOR_BLOCK_SIZE, DETECTOR_BLOCK_SIZE);
      // const { data, width, height } = block.bitmap;

      // let sum = 0;
      // for (let i = 0; i < data.length; i += 4) {
      //   sum += data[i];
      // }

      // const avg = sum / data.length;
      // const isLight = avg >= 8 / DETECTOR_BLOCK_SIZE;

      let sum = 0;
      let count = 0;
      image.scan(x, y, DETECTOR_BLOCK_SIZE, DETECTOR_BLOCK_SIZE, function (x, y, idx) {
        sum += this.bitmap.data[idx];
        count++;
      });

      const avg = sum / count;
      const isLight = avg >= DETECTOR_WHITE_THRESHOLD;

      if (isLight) {
        const cx = Math.floor(x / DETECTOR_BLOCK_SIZE / CLUSTER_SIZE);
        const cy = Math.floor(y / DETECTOR_BLOCK_SIZE / CLUSTER_SIZE);

        blocks.push({ x, y });
        saveToCluster(x, y);
      }
    }
  }

  logger.log("Separated to blocks");

  for (const block of blocks) {
    const cx = Math.floor(block.x / DETECTOR_BLOCK_SIZE / CLUSTER_SIZE);
    const cy = Math.floor(block.y / DETECTOR_BLOCK_SIZE / CLUSTER_SIZE);

    const cluster = blockClusters[`${cx},${cy}`] ?? [];

    block.isRandom =
      cluster.filter((b) => {
        return Math.abs(b.x - block.x) <= AROUND_DISTANCE && Math.abs(b.y - block.y) <= AROUND_DISTANCE;
      }).length < AROUND_MIN_COUNT;

    for (let x2 = 0; x2 < DETECTOR_BLOCK_SIZE; x2++) {
      for (let y2 = 0; y2 < DETECTOR_BLOCK_SIZE; y2++) {
        const c = block.isRandom ? 0xff0000ff : 0xffffffff;
        image.setPixelColor(c, block.x + x2, block.y + y2);
      }
    }
  }

  logger.log("Clustered blocks");

  const mainBlocks = blocks.filter((block) => !block.isRandom);

  const minX = mainBlocks.reduce((min, block) => Math.min(min, block.x), width) * upscaleX;
  const minY = mainBlocks.reduce((min, block) => Math.min(min, block.y), height) * upscaleY;
  const maxX = (mainBlocks.reduce((max, block) => Math.max(max, block.x), 0) + DETECTOR_BLOCK_SIZE) * upscaleX;
  const maxY = (mainBlocks.reduce((max, block) => Math.max(max, block.y), 0) + DETECTOR_BLOCK_SIZE) * upscaleY;

  logger.log("Calculated boundaries");
  logger.total();

  console.log(`Boundaries [${minX}, ${minY} to ${maxX}, ${maxY}] with ${mainBlocks.length} blocks`);

  return {
    minX,
    minY,
    maxX,
    maxY,
    image,
    postprocessedImage,
  };
}
