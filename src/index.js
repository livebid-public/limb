import minimist from "minimist";

import { config } from "./config.js";
import { runnerJoinPairs } from "./runner/joinPairs.js";
import { runnerSingleImage } from "./runner/singleImage.js";

async function main() {
  const argv = minimist(process.argv.slice(2));

  if (argv.flipX !== undefined) {
    config.flipImageX = !!argv.flipX;
  }

  if (argv.flipY !== undefined) {
    config.flipImageY = !!argv.flipY;
  }

  if (argv.debug !== undefined) {
    config.debug = !!argv.debug;
  }

  if (argv.inPath !== undefined) {
    config.inPath = argv.inPath;
  }

  if (argv.outPath !== undefined) {
    config.outPath = argv.outPath;
  }

  if (argv.debugPath !== undefined) {
    config.debugPath = argv.debugPath;
  }

  if (argv.removeBg !== undefined) {
    config.removeBg = !!argv.removeBg;
  }

  const mode = argv.mode || "single";

  if (mode === "pair") {
    await runnerJoinPairs();
  } else if (mode === "single") {
    await runnerSingleImage();
  } else {
    console.error(`Unknown mode ${mode}`);
  }
}

main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
