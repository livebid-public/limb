import { removeBackground } from "@imgly/background-removal-node";
import Jimp from "jimp";

import { config } from "./config.js";
import { detectImageBoundaries } from "./detector/customCPUBased.js";
import { divider } from "./utils.js";

function toArrayBuffer(buffer) {
  const arrayBuffer = new ArrayBuffer(buffer.length);
  const view = new Uint8Array(arrayBuffer);
  for (let i = 0; i < buffer.length; ++i) {
    view[i] = buffer[i];
  }
  return arrayBuffer;
}

export async function preprocessImage(image) {
  const { width, height } = image.bitmap;

  if (config.flipImageX || config.flipImageY) {
    image.flip(config.flipImageX, config.flipImageY);
  }

  if (config.removeBg) {
    const start = Date.now();
    const inputBuffer = await image.getBufferAsync(Jimp.MIME_JPEG);
    const blob = new Blob([inputBuffer], { type: "image/jpeg" });

    const blobWithoutBg = await removeBackground(blob, {
      output: {
        format: "image/png",
      },
    });

    const outputBuffer = Buffer.from(await blobWithoutBg.arrayBuffer());

    // Convert to Jimp image
    const imageWithoutBg = await Jimp.read(outputBuffer);

    const newImage = new Jimp(width, height, 0xffffffff);
    newImage.composite(imageWithoutBg, 0, 0);

    image = newImage;

    console.log(`Background removed in ${Date.now() - start}ms`);
  }

  return image;
}

export function addPadding({ minX, minY, maxX, maxY, ...rest }, dim, padding) {
  return {
    minX: Math.round(Math.max(minX - padding, 0)),
    minY: Math.round(Math.max(minY - padding, 0)),
    maxX: Math.round(Math.min(maxX + padding, dim.width)),
    maxY: Math.round(Math.min(maxY + padding, dim.height)),
    ...rest,
  };
}

export async function processFileToCroppedImage(file) {
  const fileName = file.split("/").pop();

  let image = await Jimp.read(file);

  image = await preprocessImage(image);

  const detectedBounds = await detectImageBoundaries(image);
  const bounds = addPadding(detectedBounds, { width: image.bitmap.width, height: image.bitmap.height }, config.padding);

  await image.crop(bounds.minX, bounds.minY, bounds.maxX - bounds.minX, bounds.maxY - bounds.minY);

  if (config.debug) {
    await bounds.image?.writeAsync(`${config.debugPath}/${fileName.split(".")[0]}-blocks.jpg`);
    await bounds.postprocessedImage?.writeAsync(`${config.debugPath}/${fileName.split(".")[0]}-postprocess.jpg`);
    await image.writeAsync(`${config.debugPath}/${fileName}`);
  }

  const { width, height } = image.bitmap;

  console.log(
    `Cropped [${bounds.minX}, ${bounds.minY} to ${bounds.maxX}, ${bounds.maxY}] from original [0, 0 to ${width}, ${height}]`
  );

  divider();

  return {
    image,
    bounds,
  };
}

export async function DEBUG_processFileToCroppedImage(file) {
  const fileName = file.split("/").pop();
  const baseName = fileName.split(".")[0];

  const image = await Jimp.read(file);

  // image.blur(4);
  image.resize(Jimp.AUTO, 256);
  image.greyscale();
  image.brightness(0.4);
  image.contrast(0.4);

  await image.writeAsync(`${config.debugPath}/${baseName}-preprocess.jpg`);

  // Edge detection
  image.convolution([
    [0, 1, 0],
    [1, -4, 1],
    [0, 1, 0],
  ]);

  await image.writeAsync(`${config.debugPath}/${baseName}-edge.jpg`);

  return {
    image,
    bounds: { minX: 0, minY: 0, maxX: image.bitmap.width, maxY: image.bitmap.height },
  };
}
