export const divider = () => console.log("----------");

export const logPerf = async (cb, name) => {
  const start = Date.now();
  const res = await cb();
  console.log(`${name} took ${Date.now() - start}ms`);

  return res;
};

export const createPerfLog = () => {
  const start = Date.now();
  let lastTime = start;

  return {
    log: (name) => {
      const now = Date.now();
      console.log(`${name} took ${now - lastTime}ms`);
      lastTime = now;
    },
    total: () => {
      console.log(`Total took ${Date.now() - start}ms`);
    },
  };
};
