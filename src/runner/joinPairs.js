import fs from "fs/promises";
import Jimp from "jimp";

import { config, FORBIDDEN_FILES } from "../config.js";
import { processFileToCroppedImage } from "../images.js";
import { divider } from "../utils.js";

async function findPairImages(arr) {
  let files = await fs.readdir(config.inPath);
  files = files.filter((file) => !FORBIDDEN_FILES.includes(file));

  const getKeyCode = (str) => {
    const short = str.split(".")[0];

    if (short.length === 0) {
      return "";
    }

    return short.substring(0, short.length - 1);
  };

  const getCharacter = (str) => {
    const match = str.match(/[a-zA-Z]+/);
    return match ? match[0] : null;
  };

  const pairs = {};
  for (const file of files) {
    const fileName = file.split(".")[0];
    const keyCode = getKeyCode(fileName);
    const character = getCharacter(fileName);

    // Skip files without keyCode or character
    if (keyCode === null || character === null) {
      console.log(`Skipping ${file} because it doesn't have keyCode or character`);
      continue;
    }

    // Pair already found
    if (pairs[number]) continue;

    const pairedFile = files.find((f) => {
      const fKeyCode = getKeyCode(f);
      const fCharacter = getCharacter(f);
      return fKeyCode === number && fCharacter !== character;
    });

    if (!pairedFile) {
      console.log(`No pair found for ${file}`);
      continue;
    }

    pairs[number] = {
      number,
      first: `${config.inPath}/${file}`,
      second: `${config.inPath}/${pairedFile}`,
    };
  }

  return Object.values(pairs);
}

async function joinTwoImages(image1, image2) {
  const width = Math.max(image1.bounds.maxX, image2.bounds.maxX);
  const height = Math.max(image1.bounds.maxY, image2.bounds.maxY);
  const ratio1 = image1.image.bitmap.width / image1.image.bitmap.height;
  const ratio2 = image2.image.bitmap.width / image2.image.bitmap.height;

  const newWidth1 = height * ratio1;
  const newHeight1 = height;
  const newWidth2 = height * ratio2;
  const newHeight2 = height;

  image1.image.resize(newWidth1, newHeight1);
  image2.image.resize(newWidth2, newHeight2);

  const canvas = new Jimp(newWidth1 + newWidth2, height);

  canvas.composite(image1.image, 0, 0, {
    mode: Jimp.BLEND_SOURCE_OVER,
    opacityDest: 1,
    opacitySource: 1,
  });

  canvas.composite(image2.image, newWidth1, 0, {
    mode: Jimp.BLEND_SOURCE_OVER,
    opacityDest: 1,
    opacitySource: 1,
  });

  return canvas;
}

export async function runnerJoinPairs() {
  const pairs = await findPairImages();

  divider();

  for (const pair of pairs) {
    console.log(`[${pair.number}] Processing ${pair.first}`);
    const image1 = await processFileToCroppedImage(pair.first);
    console.log(`[${pair.number}] Processing ${pair.second}`);
    const image2 = await processFileToCroppedImage(pair.second);

    console.log(`[${pair.number}] Joining images`);

    const joinedImage = await joinTwoImages(image1, image2);
    await joinedImage.writeAsync(`${config.outPath}/${pair.number}.jpg`);

    console.log(`[${pair.number}] Final image saved to ${config.outPath}/${pair.number}.jpg`);

    divider();
  }
}
