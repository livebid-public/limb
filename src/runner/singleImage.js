import fs from "fs/promises";

import { config, FORBIDDEN_FILES } from "../config.js";
import { processFileToCroppedImage } from "../images.js";
import { divider } from "../utils.js";

async function findImages() {
  const files = await fs.readdir(config.inPath);

  return files.filter((file) => !FORBIDDEN_FILES.includes(file));
}

export async function runnerSingleImage() {
  const images = await findImages();

  divider();

  for (let i = 0; i < images.length; i++) {
    const file = images[i];

    console.log(`[${i}] Processing ${file}`);
    const { image } = await processFileToCroppedImage(`${config.inPath}/${file}`);

    await image.writeAsync(`${config.outPath}/${file}`);

    console.log(`[${i}] Image saved to ${config.outPath}/${file}`);

    divider();
  }
}
